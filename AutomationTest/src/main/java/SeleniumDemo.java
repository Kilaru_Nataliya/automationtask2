import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

public class SeleniumDemo {
    public static void main(String[] args) throws InterruptedException {
        String property = System.getProperty("user.dir") + "/driver/chromedriver.exe";
        System.setProperty("webdriver.chrome.driver", property);

        WebDriver driver = new ChromeDriver();
        driver.get("http://prestashop-automation.qatestlab.com.ua/admin147ajyvk0/");

        loginTest(driver);
        checkItem(driver);


    }

    private static void loginTest(WebDriver driver) {

        WebElement emailField = driver.findElement(By.name("email"));
        emailField.sendKeys("webinar.test@gmail.com");

        WebElement passwdPassword = driver.findElement(By.name("passwd"));
        passwdPassword.sendKeys("Xcg7299bnSmMuRLp9ITw");

        WebElement loginButton = driver.findElement(By.name("submitLogin"));
        loginButton.click();
        driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);

        WebElement avatar = driver.findElement(By.xpath("//*[@id=\"employee_infos\"]/a/span/img"));
        avatar.click();

        WebElement logoutButtom = driver.findElement(By.id("header_logout"));
        logoutButtom.click();

    }

    private static void checkItem(WebDriver driver) {

        WebElement emailField = driver.findElement(By.name("email"));
        emailField.sendKeys("webinar.test@gmail.com");

        WebElement passwdPassword = driver.findElement(By.name("passwd"));
        passwdPassword.sendKeys("Xcg7299bnSmMuRLp9ITw");

        WebElement loginButton = driver.findElement(By.name("submitLogin"));
        loginButton.click();
        driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);



        WebElement order1 = driver.findElement(By.xpath("//*[@id=\"tab-AdminDashboard\"]/a/span"));
        order1.click();
        String s1 = driver.getTitle();
        System.out.println(s1);
        driver.navigate().refresh();
        Assert.assertEquals(s1, driver.getTitle());
        driver.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);


        WebElement order = driver.findElement(By.xpath("//*[@id=\"subtab-AdminParentOrders\"]/a/span"));
        order.click();
        String s = driver.getTitle();
        System.out.println(s);
        driver.navigate().refresh();
        Assert.assertEquals(s, driver.getTitle());
        driver.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);


        WebElement order2 = driver.findElement(By.xpath("//*[@id=\"subtab-AdminCatalog\"]/a/span"));
        order2.click();
        String s2 = driver.getTitle();
        System.out.println(s2);
        driver.navigate().refresh();
        Assert.assertEquals(s2, driver.getTitle());
        driver.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);


        WebElement order3 = driver.findElement(By.xpath(".//li[@data-submenu=\"23\"]"));
        order3.click();
        String s3 = driver.getTitle();
        System.out.println(s3);
        driver.navigate().refresh();
        Assert.assertEquals(s3, driver.getTitle());
        driver.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);


        WebElement order4 = driver.findElement(By.xpath("//*[@id=\"subtab-AdminParentCustomerThreads\"]/a/span"));
        order4.click();
        String s4 = driver.getTitle();
        System.out.println(s4);
        driver.navigate().refresh();
        Assert.assertEquals(s4, driver.getTitle());
        driver.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);


        WebElement order5 = driver.findElement(By.xpath("//*[@id=\"subtab-AdminStats\"]/a/span"));
        order5.click();
        String s5 = driver.getTitle();
        System.out.println(s5);
        driver.navigate().refresh();
        Assert.assertEquals(s5, driver.getTitle());
        driver.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);


        WebElement order6 = driver.findElement(By.xpath("//*[@id=\"subtab-AdminParentModulesSf\"]/a/span"));
        order6.click();
        String s6 = driver.getTitle();
        System.out.println(s6);
        driver.navigate().refresh();
        Assert.assertEquals(s6, driver.getTitle());
        driver.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);


        WebElement order7 = driver.findElement(By.xpath(".//li[@data-submenu=\"46\"]"));
        order7.click();
        String s7 = driver.getTitle();
        System.out.println(s7);
        driver.navigate().refresh();
        Assert.assertEquals(s7, driver.getTitle());
        driver.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);


        WebElement order8 = driver.findElement(By.xpath("//*[@id=\"subtab-AdminParentShipping\"]/a/span"));
        order8.click();
        String s8 = driver.getTitle();
        System.out.println(s8);
        driver.navigate().refresh();
        Assert.assertEquals(s8, driver.getTitle());
        driver.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);


        WebElement order9 = driver.findElement(By.xpath("//*[@id=\"subtab-AdminParentPayment\"]/a"));
        order9.click();
        String s9 = driver.getTitle();
        System.out.println(s9);
        driver.navigate().refresh();
        Assert.assertEquals(s9, driver.getTitle());
        driver.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);


        WebElement order10 = driver.findElement(By.xpath("//*[@id=\"subtab-AdminInternational\"]/a/span"));
        order10.click();
        String s10 = driver.getTitle();
        System.out.println(s10);
        driver.navigate().refresh();
        Assert.assertEquals(s10, driver.getTitle());
        driver.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);


        WebElement order11 = driver.findElement(By.xpath("//*[@id=\"subtab-ShopParameters\"]/a/span"));
        order11.click();
        String s11 = driver.getTitle();
        System.out.println(s11);
        driver.navigate().refresh();
        Assert.assertEquals(s11, driver.getTitle());
        driver.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);


        WebElement order12 = driver.findElement(By.xpath("//*[@id=\"subtab-AdminAdvancedParameters\"]/a/span"));
        order12.click();
        String s12 = driver.getTitle();
        System.out.println(s12);
        driver.navigate().refresh();
        Assert.assertEquals(s12, driver.getTitle());


    }

}